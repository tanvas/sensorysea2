﻿using UnityEngine;

public class PlayerScript : MonoBehaviour
{
    float waitTime;
    //public AnimationClip animationclip;

    public float upForce;                   //Upward force of the "flap".
    private bool isDead = false;            //Has the player collided with a wall?

   // private Animator anim;                  //Reference to the Animator component.
    private Rigidbody2D rb2d;				//Holds a reference to the Rigidbody2D component of the Player.

    Transform target;
    float speed = 6f;
    Vector2 targetPos;

    public bool firstTouch = false;

    private float XposOnDeath = 0;

    public AudioClip playerHitWallSFX;
    private AudioSource audioSource;

    void Start()
    {
        //Get reference to the Animator component attached to this GameObject.
        //anim = GetComponent<Animator>();
        //Get and store a reference to the Rigidbody2D attached to this GameObject.
        rb2d = GetComponent<Rigidbody2D>();

        targetPos = transform.position;

        //waitTime = animationclip.length + 3f;
        //InvokeRepeating("PlayAnim", 3f, waitTime);


        audioSource = GetComponent<AudioSource>(); 
    }


    void Update()
    {
        //Don't allow control if the Player has died.
        if (isDead == false)
        {
            if (Input.GetMouseButton(0))
            {
                if (GameControl.instance.gameStart == false && firstTouch == false)
                {
                    GameControl.instance.GameStart();
                    firstTouch = true;
                }
                targetPos = (Vector2)Camera.main.ScreenToWorldPoint(Input.mousePosition);
            }
            if ((Vector2)transform.position != targetPos)
            {
                transform.position = Vector2.MoveTowards(transform.position, new Vector2(targetPos.x, transform.position.y), speed * Time.deltaTime);
            }
        }
        if (isDead)
        {
            transform.position = Vector2.MoveTowards(transform.position, new Vector2(XposOnDeath, -6), 12f * Time.deltaTime);
        }
    }

    void OnCollisionEnter2D(Collision2D other)
    {
        // If the Player collides with something set it to dead...
        isDead = true;
        audioSource.PlayOneShot(playerHitWallSFX);

        XposOnDeath = transform.position.x;
        gameObject.GetComponent<Collider2D>().enabled = false;
        gameObject.GetComponent<Rigidbody2D>().isKinematic = true;

        GameControl.instance.PlayerDied();
    }

}
