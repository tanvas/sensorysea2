﻿using UnityEngine;

public class Column : MonoBehaviour 
{
    public bool hitOnce = false;

    void Start()
    {
        hitOnce = false;
    }

	void OnTriggerEnter2D(Collider2D other)
	{
		if(other.GetComponent<PlayerScript>() != null && hitOnce == false)
		{
			//If the Player hits the trigger collider in between the columns then
			//tell the game control that the Player scored.
			GameControl.instance.PlayerScored();
            hitOnce = true;
		}
	}


}
