﻿using UnityEngine;
using UnityEngine.UI;

public class GameControl : MonoBehaviour 
{
	public static GameControl instance;			//A reference to our game control script so we can access it statically.
	public Text scoreText;						//A reference to the UI text component that displays the player's score.
	public GameObject gameOvertext;				//A reference to the object that displays the text which appears when the player dies.
    public GameObject instructionText;

	private int score = 0;						//The player's score.
	public bool gameOver = false;				//Is the game over?
    public bool gameStart = false;

    public AudioClip scoreSFX;
    private AudioSource audioSource;


	void Awake()
	{
		//If we don't currently have a game control...
		if (instance == null)
			//...set this one to be it...
			instance = this;
		//...otherwise...
		else if(instance != this)
			//...destroy this one because it is a duplicate.
			Destroy (gameObject);

        audioSource = GetComponent<AudioSource>();
	}

    public void LoadScene(int level)
    {
        Application.LoadLevel(level);
    }

    public void PlayerScored()
	{
		//The Player can't score if the game is over.
		if (gameOver)	
			return;
		//If the game is not over, increase the score...
		score++;
        audioSource.PlayOneShot(scoreSFX);
		//...and adjust the score text.
		scoreText.text = "Score: " + score.ToString();
	}

	public void PlayerDied()
	{
		//Activate the game over text.
		gameOvertext.SetActive (true);
		//Set the game to be over.
		gameOver = true;
	}

    public void GameStart()
    {
        instructionText.SetActive(false);
        gameStart = true;
    }
}
