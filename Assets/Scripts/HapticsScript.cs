﻿using UnityEngine;
using TanvasTouch.Model;

public class HapticsScript : MonoBehaviour {

    private HapticView hapticView;
    private HapticTexture mHapticTexture;
    private HapticMaterial mHapticMaterial;
    private HapticSprite hapticSprite;

    //Called at start of application.
    void Start()
    {
        //Connect to the service and begin intializing the haptic resources.
        InitHaptics();
    }

    void InitHaptics()
    {
        HapticServiceAdapter hapticServiceAdapter = HapticServiceAdapter.GetInstance();

        if (hapticServiceAdapter != null)
        {
            hapticView = HapticView.Create(hapticServiceAdapter);
            hapticView.Activate();
            hapticView.SetOrientation(Screen.orientation);

            Texture2D texture = Resources.Load("safe_haptics") as Texture2D;
            byte[] textureData = TanvasTouch.HapticUtil.CreateHapticDataFromTexture(texture, TanvasTouch.HapticUtil.Mode.Brightness);

            HapticTexture hapticTexture = HapticTexture.Create(hapticServiceAdapter);
            hapticTexture.SetSize(texture.width, texture.height);
            hapticTexture.SetData(textureData);

            HapticMaterial hapticMaterial = HapticMaterial.Create(hapticServiceAdapter);
            hapticMaterial.SetTexture(0, hapticTexture);

            hapticSprite = HapticSprite.Create(hapticServiceAdapter);
            hapticSprite.SetMaterial(hapticMaterial);
            hapticSprite.SetSize(texture.width, texture.height);
            hapticSprite.SetPosition(0, 0);

            hapticView.AddSprite(hapticSprite);
            hapticSprite.SetEnabled(false);           
        }
    }

    public void UpdateSpritePosition(int x, int y)
    {
        if (hapticSprite != null)
        {
            hapticSprite.SetPosition(x, y);
        }
    }

    public void SetX(int x)
    {
        if (hapticSprite != null)
        {
            hapticSprite.SetX(x);
        }

    }

    public void SetEnabled(bool enabled)
    {
        if (hapticSprite != null)
        {
            hapticSprite.SetEnabled(enabled);
        }
    }

    void OnDestroy()
    {
        if (hapticView != null)
        {
            hapticView.RemoveAllSprites();
            hapticView.Deactivate();
        }
    }
}
