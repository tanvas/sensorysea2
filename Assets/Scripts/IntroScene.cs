﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IntroScene : MonoBehaviour {

    public GameObject amandaLove;
    public GameObject amandaGlare;
    public GameObject lloydLove;
    public GameObject lloydSurprise;
    public GameObject push;

    int tapCount;
    float doubleTapTimer;

    // Use this for initialization
    void Start () {

        Invoke("AmandaLove", 7f);
		
	}

    // Update is called once per frame
    void Update () {


        if (Input.touchCount == 1 && Input.GetTouch(0).phase == TouchPhase.Began)
        {
            tapCount++;
        }
        if (tapCount > 0)
        {
            doubleTapTimer += Time.deltaTime;
        }
        if (tapCount >= 2)
        {
            Application.LoadLevel("Main");
            doubleTapTimer = 0.0f;
            tapCount = 0;
        }
        if (doubleTapTimer > 0.5f)
        {
            doubleTapTimer = 0f;
            tapCount = 0;
        }

    }

    public void AmandaLove()
    {
        amandaLove.SetActive(true);
        Invoke("LloydLove", 4);
    }

    public void LloydLove()
    {
        amandaLove.SetActive(false);
        lloydLove.SetActive(true);
        Invoke("AmandaGlare", 4);
    }

    public void AmandaGlare()
    {
        lloydLove.SetActive(false);
        amandaGlare.SetActive(true);
        Invoke("LloydScared", 1);
    }

    public void LloydScared()
    {
        amandaGlare.SetActive(false);
        lloydSurprise.SetActive(true);
        Invoke("Push", 1);
    }

    public void Push()
    {
        lloydSurprise.SetActive(false);
        push.SetActive(true);
        Invoke("StartGame", 2.5f);
    }

    public void StartGame()
    {
        Application.LoadLevel("Main");
    }
}
